﻿using eFaktoria.BLL.Services.InvoicePosistion;
using eFaktoria.Model.DTO.InvoicePosition;
using System.Web.Http;

namespace eFaktoria.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("backend/InvoicePosition")]
    public class InvoicePositionController : _WebApiBaseController
    {
        private readonly IInvoicePositionDictionaryManagementService _invoicePositionManagementService;
        public InvoicePositionController(IInvoicePositionDictionaryManagementService invoicePositionManagementService)
        {
            _invoicePositionManagementService = invoicePositionManagementService;
        }

        [HttpGet]
        [Route("list")]
        public IHttpActionResult GetList()
        {
            var result = UseService(() =>
                _invoicePositionManagementService.GetList(UserId));

            return result;
        }

        [HttpPost]
        [Route("create")]
        public IHttpActionResult Create(CreateInvoicePositionDictionaryDTO model)
        {
            var result = UseService(() =>
                _invoicePositionManagementService.Create(model, UserId));

            return result;
        }

        [HttpPut]
        [Route("update")]
        public IHttpActionResult Update(UpdateInvoicePositionDictionaryDTO model)
        {
            var result = UseService(() =>
                _invoicePositionManagementService.Update(model, UserId));

            return result;
        }

        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult Delete(int id)
        {
            var result = UseService(() =>
                _invoicePositionManagementService.Delete(id, UserId));

            return result;
        }
    }
}
