﻿using eFaktoria.BLL.Services.Company;
using eFaktoria.Model.DTO.Company;
using System.Web.Http;

namespace eFaktoria.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("backend/bankAccount")]
    public class BankAccountController : _WebApiBaseController
    {
        private readonly IBankAccountService _bankAccountService;
        public BankAccountController(IBankAccountService bankAccountService)
        {
            _bankAccountService = bankAccountService;
        }

        [HttpGet]
        [Route("list")]
        public IHttpActionResult GetList()
        {
            var result = UseService(() => _bankAccountService.GetList(UserId));
            return result;
        }


        [HttpPut]
        [Route("create")]
        public IHttpActionResult Create(CreateBankAccountDTO model)
        {
            var result = UseService(() => _bankAccountService.CreateBankAccount(model, UserId));
            return result;
        }

        [HttpPut]
        [Route("setMain")]
        public IHttpActionResult SetToMain(int id)
        {
            var result = UseService(() => _bankAccountService.SetToMain(id, UserId));
            return result;
        }

        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult Delete(int id)
        {
            var result = UseService(() => _bankAccountService.DeleteBankAccount(id, UserId));
            return result;
        }
    }
}
