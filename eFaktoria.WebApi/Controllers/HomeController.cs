﻿using eFaktoria.BLL.Services;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;

namespace eFaktoria.WebApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITestService _testService;
        public HomeController(ITestService testService)
        {
            _testService = testService;
        }
        public ActionResult Index()
        {
            var a = _testService.GetSomethingFromDb();
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [Authorize]
        public ActionResult Data()
        {
            return View(User.Identity.GetUserId());
        }
    }
}
