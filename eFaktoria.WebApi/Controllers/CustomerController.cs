﻿using eFaktoria.BLL.Services.Customer;
using eFaktoria.Model.DTO.Customer;
using System.Web.Http;

namespace eFaktoria.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("backend/customer")]
    public class CustomerController : _WebApiBaseController
    {
        private readonly ICustomerManagementService _customerManagementService;
        private readonly ICustomerListService _customerListService;

        public CustomerController(ICustomerManagementService customerManagementService, ICustomerListService customerListService)
        {
            _customerManagementService = customerManagementService;
            _customerListService = customerListService;
        }

        /// <summary>
        /// Pobiera listę faktur
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("list")]
        public IHttpActionResult GetList(int pageNumber, int pageSize)
        {
            var list = UseService(() => _customerListService.GetCustomerList(UserId, pageNumber, pageSize));

            return list;
        }

        /// <summary>
        /// Pobiera dane klienta wraz z listą faktur
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("data")]
        public IHttpActionResult GetDetails(int id)
        {
            var data = UseService(() => _customerManagementService.GetDetails(UserId, id));

            return data;
        }

        /// <summary>
        /// Usuwa kontrahenta
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete")]
        public IHttpActionResult Delete(int id)
        {
            var data = UseService(() => _customerManagementService.DeleteCustomer(UserId, id));

            return data;
        }

        /// <summary>
        /// Dodaje nowego klienta
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        public IHttpActionResult Create(CreateCustomerDTO model)
        {
            var result = UseService(() => _customerManagementService.CreateNewCustomer(UserId, model));

            return result;
        }

        /// <summary>
        /// Aktualizuje dane klienta
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Route("update")]
        public IHttpActionResult Update(UpdateCustomerDTO model)
        {
            var result = UseService(() => _customerManagementService.UpdateCustomer(UserId, model));

            return result;
        }
    }
}
