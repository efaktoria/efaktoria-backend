﻿using Autofac;
using System.Reflection;

namespace eFaktoria.WebApi.Modules
{
    public class EFModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.Load("eFaktoria.Core"))
                .Where(t => t.Name.EndsWith("Factory"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterAssemblyTypes(Assembly.Load("eFaktoria.BLL"))
                .Where(t => t.Name.EndsWith("Context"))
                .InstancePerLifetimeScope();


            builder.RegisterAssemblyTypes(Assembly.Load("eFaktoria.Core"))
                .Where(t => t.Name.EndsWith("Locator"))
                .AsImplementedInterfaces()
                .InstancePerRequest();

            builder.RegisterAssemblyTypes(Assembly.Load("eFaktoria.Core"))
                .Where(t => t.Name.EndsWith("Scope"))
                .AsImplementedInterfaces()
                .InstancePerRequest();
        }

    }
}