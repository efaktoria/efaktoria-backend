﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace eFaktoria.Model.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class NIPAttribute : ValidationAttribute
    {
        private readonly int[] _multipliers = { 6, 5, 7, 2, 3, 4, 5, 6, 7 };

        public override bool IsValid(object value)
        {
            if (value == null) return true;

            var nip = value as string;

            if (nip == null || !Regex.IsMatch(nip, @"^[\d]{10}$")) return false;

            var sum = nip.Select((t, i) => i < nip.Length - 1 ? _multipliers[i] * int.Parse(nip.Substring(i, 1)) : 0)
                .Sum();

            return sum % 11 != 10 && sum % 11 == int.Parse(nip[nip.Length - 1].ToString());
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(CultureInfo.CurrentCulture,
                "Podany numer NIP nie jest prawidłowy");
        }
    }
}