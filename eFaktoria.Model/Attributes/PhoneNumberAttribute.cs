﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text.RegularExpressions;

namespace eFaktoria.Model.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class PhoneNumberAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var phone = (String)value;
            bool result = true;
            var match = Regex.IsMatch(phone, @"^(?:\(?\+?48)?(?:[-\.\(\)\s]*(\d)){9}\)?$");
            if (!match)
            {
                result = false;
            }
            return result;
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture,
                "Nieprawidłowy format numeru telefonu");
        }
    }
}
