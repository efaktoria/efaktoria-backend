﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text.RegularExpressions;

namespace eFaktoria.Model.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class PostalCodeAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool result = true;

            try
            {
                var postalCodeValue = (string)value;

                var resultRegex = Regex.IsMatch(postalCodeValue, @"[0-9]{2}-[0-9]{3}");

                if (!resultRegex)
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture,
                "Nieprawdiłowy format kodu pocztowego");
        }
    }
}
