﻿using System.ComponentModel.DataAnnotations;

namespace eFaktoria.Model.DTO.InvoicePosition
{
    public class UpdateInvoicePositionDictionaryDTO
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie nazwy pozycji faktury")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie ilości")]
        public int Count { get; set; }

        /// <summary>
        /// Jednostka
        /// </summary>
        [Required(ErrorMessage = "Wymagane jest podanie jednostki")]
        public string Unit { get; set; }

        /// <summary>
        /// stawka vat
        /// </summary>
        [Required(ErrorMessage = "Wymagane jest podanie stawki VAT")]
        public int VatRateId { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie ceny netto")]
        public decimal NetPrice { get; set; }
    }
}
