﻿using eFaktoria.Core.Helpers;
using System.Collections.Generic;

namespace eFaktoria.Model.DTO.InvoicePosition
{
    public class InvoicePositionDictionaryListItemDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        /// <summary>
        /// Jednostka
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// stawka vat
        /// </summary>
        public int VatRateId { get; set; }

        public decimal? NetPrice { get; set; }

        public decimal? NetValue => NetPrice != null ? Count * NetPrice : null;

        public decimal GrossValue
        {
            get
            {
                if (NetValue != null)
                {
                    return GrossValueHelper.GetValue(new Dictionary<int, decimal>
                    {
                        {VatRateId, NetValue.Value}
                    });
                }

                return 0;
            }
        }
    }
}
