﻿using eFaktoria.Core.Helpers;
using System;
using System.Collections.Generic;

namespace eFaktoria.Model.DTO.Invoices
{
    public class InvoiceListItemDTO
    {
        public bool IsPaid { get; set; }

        /// <summary>
        /// Data płatności
        /// </summary>
        public DateTime PaymentDate { get; set; }

        public string Title { get; set; }

        public string ContractorName { get; set; }

        public decimal GrossValue => GrossValueHelper.GetValue(PositionsNetValuesWithTax);

        /// <summary>
        /// Słownik z kwotami poszczególnych pozycji faktury z id stawki, której dotyczy
        /// </summary>
        public Dictionary<int, decimal> PositionsNetValuesWithTax { private get; set; }
    }
}
