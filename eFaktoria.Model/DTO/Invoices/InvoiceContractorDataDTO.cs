﻿namespace eFaktoria.Model.DTO.Invoices
{
    public class InvoiceContractorDataDTO
    {
        public string Address { get; set; }
        public string CompanyName { get; set; }
        public string NIP { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
    }
}