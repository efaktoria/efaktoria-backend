﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace eFaktoria.Model.DTO.Invoices
{
    public class CreateInvoiceDTO
    {
        public CreateInvoiceDTO()
        {
            Positions = new List<InvoicePositionDTO>();
        }

        public List<InvoicePositionDTO> Positions { get; set; }

        [Required(ErrorMessage = "Podaj miejsce wystawienia faktury")]
        public string City { get; set; }

        [Required(ErrorMessage = "Podaj osobę upoważnioną do wystawienia faktury")]
        public string AuthorizedPerson { get; set; }

        [Required(ErrorMessage = "Wybierz konto bankowe")]
        public int AccountBankId { get; set; }

        public string Remarks { get; set; }

        [Required(ErrorMessage = "Wybierz walutę")]
        public int CurrencyId { get; set; }

        public int ContractorId { get; set; }

        /// <summary>
        /// Data sprzedaży
        /// </summary>
        [Required(ErrorMessage = "Podaj datę sprzedaży")]
        public DateTime SellDate { get; set; }

        /// <summary>
        /// Data płatnośći
        /// </summary>
        [Required(ErrorMessage = "Podaj datę płatności")]
        public DateTime PaymentDate { get; set; }

        /// <summary>
        /// Data wystawienia
        /// </summary>
        [Required(ErrorMessage = "Podaj datę wystawienia faktury")]
        public DateTime IssueDate { get; set; }

        [Required(ErrorMessage = "Wybierz metodę płatności")]
        public int PaymentMethodId { get; set; }

        //[Required(ErrorMessage = "Wybierz typ faktury")]
        public int InvoiceTypeId { get; set; }

        public bool IsPaid { get; set; }

        [Required(ErrorMessage = "Podaj tytuł faktury")]
        public string Title { get; set; }
    }

    public class InvoicePositionDTO
    {
        [Required(ErrorMessage = "Wymagane jest podanie nazwy pozycji faktury")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie ilości")]
        public int Count { get; set; }

        /// <summary>
        /// Jednostka
        /// </summary>
        [Required(ErrorMessage = "Wymagane jest podanie jednostki")]
        public string Unit { get; set; }

        /// <summary>
        /// stawka vat
        /// </summary>
        [Required(ErrorMessage = "Wymagane jest podanie stawki VAT")]
        public int VatRateId { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie ceny netto")]
        public decimal NetPrice { get; set; }

        public decimal NetValue => Count * NetPrice;
    }
}
