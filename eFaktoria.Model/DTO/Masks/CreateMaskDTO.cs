﻿using System.ComponentModel.DataAnnotations;

namespace eFaktoria.Model.DTO.Masks
{
    public class CreateMaskDTO
    {
        [Required(ErrorMessage = "Wypełnij wzór")]
        public string Mask { get; set; }

        public int StartNumber { get; set; }

        public int InvoiceTypeId { get; set; }

        public bool IsMain { get; set; }

        [Required(ErrorMessage = "Podaj nazwę wzoru")]
        public string Name { get; set; }
    }
}
