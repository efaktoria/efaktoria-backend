﻿namespace eFaktoria.Model.DTO.Customer
{
    public class CustomerListItemDTO
    {
        public string CompanyName { get; set; }

        public string Address { get; set; }

        public string NIP { get; set; }

        public string PostalCode { get; set; }

        public int Id { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
