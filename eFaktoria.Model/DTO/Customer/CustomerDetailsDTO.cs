﻿using eFaktoria.Core.Helpers;
using System;
using System.Collections.Generic;

namespace eFaktoria.Model.DTO.Customer
{
    public class CustomerDetailsDTO
    {
        public CustomerDetailsDTO()
        {
            Invoices = new List<InvoiceListCustomerDTO>();
        }

        public string CompanyName { get; set; }

        public string Address { get; set; }

        public string NIP { get; set; }

        public string PostalCode { get; set; }

        public int Id { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public List<InvoiceListCustomerDTO> Invoices { get; set; }
    }

    public class InvoiceListCustomerDTO
    {
        public DateTime Date { get; set; }

        public DateTime PaymentDate { get; set; }

        public decimal GrossValue => GrossValueHelper.GetValue(PositionsNetValuesWithTax);

        /// <summary>
        /// Słownik z kwotami poszczególnych pozycji faktury z id stawki, której dotyczy
        /// </summary>
        public Dictionary<int, decimal> PositionsNetValuesWithTax { private get; set; }
    }
}
