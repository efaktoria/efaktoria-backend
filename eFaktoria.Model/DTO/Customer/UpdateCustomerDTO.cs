﻿using eFaktoria.Model.Attributes;
using System.ComponentModel.DataAnnotations;

namespace eFaktoria.Model.DTO.Customer
{
    public class UpdateCustomerDTO
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie nazwy firmy")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie numeru NIP")]
        [NIP]
        public string NIP { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie kodu pocztowego")]
        [PostalCode]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie miasta")]
        public string City { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie numeru telefonu")]
        [DataType(DataType.PhoneNumber)]
        [PhoneNumber]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie adresu e-mail")]
        [EmailAddress(ErrorMessage = "Nieprawidłowy adres e-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Wymagane jest podanie adresu")]
        public string Address { get; set; }
    }
}
