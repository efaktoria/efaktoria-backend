﻿using System.ComponentModel.DataAnnotations;

namespace eFaktoria.Model.DTO.Company
{
    public class CreateBankAccountDTO
    {
        [Required]
        [StringLength(24, MinimumLength = 24, ErrorMessage = "Nieprawidłowa długość numeru konta bankowego")]
        public string AccountNumber { get; set; }

        [Required(ErrorMessage = "Nie wybrano banku")]
        public int BankId { get; set; }

        public bool IsMain { get; set; }
    }
}
