﻿namespace eFaktoria.Model.DTO.Company
{
    public class BankAccountListItemDTO
    {
        public string BankName { get; set; }

        public int Id { get; set; }

        public string AccountNumber { get; set; }

        public bool IsMain { get; set; }
    }
}
