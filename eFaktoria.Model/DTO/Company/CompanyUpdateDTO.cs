﻿using eFaktoria.Model.Attributes;
using System.ComponentModel.DataAnnotations;

namespace eFaktoria.Model.DTO.Company
{
    public class CompanyUpdateDTO
    {
        [Required]
        public string CompanyName { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [PostalCode]
        public string PostalCode { get; set; }

        [Required]
        public string City { get; set; }

        public string NIP { get; set; }
    }
}
