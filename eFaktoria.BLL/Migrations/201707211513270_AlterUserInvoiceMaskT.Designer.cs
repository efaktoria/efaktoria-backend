// <auto-generated />
namespace eFaktoria.BLL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AlterUserInvoiceMaskT : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AlterUserInvoiceMaskT));
        
        string IMigrationMetadata.Id
        {
            get { return "201707211513270_AlterUserInvoiceMaskT"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
