namespace eFaktoria.BLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterContractorTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contractors", "IsActive", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Contractors", "NIP", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Contractors", "NIP", c => c.Int(nullable: false));
            DropColumn("dbo.Contractors", "IsActive");
        }
    }
}
