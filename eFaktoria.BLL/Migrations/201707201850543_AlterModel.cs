namespace eFaktoria.BLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterModel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AccountBanks", "Number", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AccountBanks", "Number", c => c.Long(nullable: false));
        }
    }
}
