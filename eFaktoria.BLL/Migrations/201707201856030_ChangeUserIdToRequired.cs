namespace eFaktoria.BLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeUserIdToRequired : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.AccountBanks", new[] { "UserId" });
            DropIndex("dbo.Invoices", new[] { "UserId" });
            DropIndex("dbo.Contractors", new[] { "UserId" });
            DropIndex("dbo.InvoicePositionDictionaries", new[] { "UserId" });
            AlterColumn("dbo.AccountBanks", "UserId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Invoices", "UserId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Contractors", "UserId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.InvoicePositionDictionaries", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.AccountBanks", "UserId");
            CreateIndex("dbo.Invoices", "UserId");
            CreateIndex("dbo.Contractors", "UserId");
            CreateIndex("dbo.InvoicePositionDictionaries", "UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.InvoicePositionDictionaries", new[] { "UserId" });
            DropIndex("dbo.Contractors", new[] { "UserId" });
            DropIndex("dbo.Invoices", new[] { "UserId" });
            DropIndex("dbo.AccountBanks", new[] { "UserId" });
            AlterColumn("dbo.InvoicePositionDictionaries", "UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Contractors", "UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Invoices", "UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.AccountBanks", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.InvoicePositionDictionaries", "UserId");
            CreateIndex("dbo.Contractors", "UserId");
            CreateIndex("dbo.Invoices", "UserId");
            CreateIndex("dbo.AccountBanks", "UserId");
        }
    }
}
