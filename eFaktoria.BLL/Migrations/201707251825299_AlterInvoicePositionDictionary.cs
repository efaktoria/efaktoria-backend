namespace eFaktoria.BLL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterInvoicePositionDictionary : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvoicePositionDictionaries", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvoicePositionDictionaries", "IsActive");
        }
    }
}
