﻿using eFaktoria.Core.EntityFramework.BaseModels;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eFaktoria.BLL.DB.Entities
{
    public class Contractor : AuditableEntity<int>
    {
        public string CompanyName { get; set; }

        public string NIP { get; set; }

        public string Address { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public bool IsActive { get; set; }
    }
}
