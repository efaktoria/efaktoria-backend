﻿using eFaktoria.Core.EntityFramework.BaseModels;
using System.ComponentModel.DataAnnotations.Schema;

namespace eFaktoria.BLL.DB.Entities
{
    public class InvoicePosition : AuditableEntity<int>
    {
        public string Name { get; set; }

        public int Count { get; set; }

        /// <summary>
        /// Jednostka
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// stawka vat
        /// </summary>
        public int VatRateId { get; set; }

        public decimal NetPrice { get; set; }

        public decimal NetValue { get; set; }

        public int InvoiceId { get; set; }

        [ForeignKey("VatRateId")]
        public virtual DictionaryPosition VatRate { get; set; }

        [ForeignKey("InvoiceId")]
        public virtual Invoice Invoice { get; set; }
    }
}
