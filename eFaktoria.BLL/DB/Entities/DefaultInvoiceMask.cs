﻿using eFaktoria.Core.EntityFramework.BaseModels;
using System.ComponentModel.DataAnnotations.Schema;

namespace eFaktoria.BLL.DB.Entities
{
    public class DefaultInvoiceMask : AuditableEntity<int>
    {
        public string Value { get; set; }

        public int InvoiceTypeId { get; set; }

        [ForeignKey("InvoiceTypeId")]
        public virtual DictionaryPosition InvoiceType { get; set; }
    }
}
