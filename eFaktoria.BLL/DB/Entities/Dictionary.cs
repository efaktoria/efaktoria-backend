﻿using eFaktoria.Core.EntityFramework.BaseModels;
using System.Collections.Generic;

namespace eFaktoria.BLL.DB.Entities
{
    public class Dictionary : AuditableEntity<int>
    {
        public string Name { get; set; }

        public virtual ICollection<DictionaryPosition> DictionaryPositions { get; set; }
    }
}
