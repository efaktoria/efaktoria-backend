﻿using eFaktoria.Core.EntityFramework.BaseModels;

namespace eFaktoria.BLL.DB.Entities
{
    public class DictionaryPosition : AuditableEntity<int>
    {
        public string Name { get; set; }

        public int DictionaryId { get; set; }

        public virtual Dictionary Dictionary { get; set; }
    }
}
