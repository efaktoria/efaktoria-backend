﻿using eFaktoria.Core.EntityFramework.BaseModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eFaktoria.BLL.DB.Entities
{
    public class AccountBank : AuditableEntity<int>
    {
        public string Number { get; set; }

        public bool IsMain { get; set; }

        public bool IsActive { get; set; }

        public int BankId { get; set; }

        [Required]
        public string UserId { get; set; }

        [ForeignKey("BankId")]
        public DictionaryPosition Bank { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public ICollection<Invoice> Invoices { get; set; }
    }
}
