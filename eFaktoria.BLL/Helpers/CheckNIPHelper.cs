﻿using System.Linq;
using System.Text.RegularExpressions;

namespace eFaktoria.BLL.Helpers
{
    public static class CheckNIPHelper
    {
        private static readonly int[] Multipliers = { 6, 5, 7, 2, 3, 4, 5, 6, 7 };

        public static bool IsValid(string nip)
        {
            if (nip == null || !Regex.IsMatch(nip, @"^[\d]{10}$")) return false;

            int sum = nip.Select((t, i) => i < nip.Length - 1 ? Multipliers[i] * int.Parse(nip.Substring(i, 1)) : 0)
                .Sum();

            return ((sum % 11) != 10 && ((sum % 11) == int.Parse(nip[nip.Length - 1].ToString())));
        }
    }
}
