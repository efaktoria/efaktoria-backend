﻿using eFaktoria.Core.EntityFramework.Infrastructure;
using eFaktoria.Core.Helpers;
using eFaktoria.Model;
using eFaktoria.Model.DTO.Customer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eFaktoria.BLL.Services.Customer
{
    public interface ICustomerListService
    {
        ResultModel<List<IssueInvoiceCustomerListItemDTO>> GetCustomerListToIssueInvoice(string userId);
        ResultModel<List<CustomerListItemDTO>> GetCustomerList(string userId, int pageNumber, int pageSize);
    }

    public class CustomerListService : GenericService, ICustomerListService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public CustomerListService(IAmbientDbContextLocator ambientDbContextLocator,
            IDbContextScopeFactory dbContextScopeFactory) : base(ambientDbContextLocator)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public ResultModel<List<CustomerListItemDTO>> GetCustomerList(string userId, int pageNumber, int pageSize)
        {
            var result = new ResultModel<List<CustomerListItemDTO>>();

            try
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var list = DB.Contractors.Where(x => x.UserId == userId && x.IsActive).Select(
                        x => new CustomerListItemDTO
                        {
                            CompanyName = x.CompanyName,
                            Id = x.Id,
                            Address = x.Address,
                            NIP = x.NIP,
                            PostalCode = x.PostalCode,
                            City = x.City,
                            PhoneNumber = x.PhoneNumber,
                            Email = x.Email
                        }).OrderBy(x => x.CompanyName).Page(pageSize, pageNumber).ToList();

                    result.Data = list;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }


        public ResultModel<List<IssueInvoiceCustomerListItemDTO>> GetCustomerListToIssueInvoice(string userId)
        {
            var result = new ResultModel<List<IssueInvoiceCustomerListItemDTO>>();

            try
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var list = DB.Contractors.Where(x => x.UserId == userId && x.IsActive).Select(
                        x => new IssueInvoiceCustomerListItemDTO
                        {
                            CompanyName = x.CompanyName,
                            Id = x.Id,
                            Address = x.Address,
                            NIP = x.NIP,
                            PostalCode = x.PostalCode,
                            City = x.City
                        }).ToList();

                    result.Data = list;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }
    }
}