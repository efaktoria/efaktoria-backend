﻿using eFaktoria.BLL.DB.Entities;
using eFaktoria.Core.EntityFramework.Infrastructure;
using eFaktoria.Model;
using eFaktoria.Model.DTO.Customer;
using System;
using System.Data.Entity.Migrations;
using System.Linq;

namespace eFaktoria.BLL.Services.Customer
{
    public interface ICustomerManagementService
    {
        ResultModel<bool> UpdateCustomer(string userId, UpdateCustomerDTO dto);
        ResultModel<bool> CreateNewCustomer(string userId, CreateCustomerDTO dto);
        ResultModel<bool> DeleteCustomer(string userId, int id);
        ResultModel<CustomerDetailsDTO> GetDetails(string userId, int id);
    }
    public class CustomerManagementService : GenericService, ICustomerManagementService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public CustomerManagementService(IAmbientDbContextLocator ambientDbContextLocator, IDbContextScopeFactory dbContextScopeFactory) : base(ambientDbContextLocator)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public ResultModel<CustomerDetailsDTO> GetDetails(string userId, int id)
        {
            var result = new ResultModel<CustomerDetailsDTO>();
            try
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var customerDetails =
                        DB.Contractors.Where(x => x.Id == id && x.IsActive && x.UserId.Equals(userId)).Select(x => new CustomerDetailsDTO
                        {
                            Id = x.Id,
                            Address = x.Address,
                            City = x.City,
                            CompanyName = x.CompanyName,
                            Email = x.Email,
                            NIP = x.NIP,
                            PostalCode = x.PostalCode,
                            PhoneNumber = x.PhoneNumber
                        }).FirstOrDefault();

                    if (customerDetails == null)
                    {
                        result.SetNotFoundError();
                        return result;
                    }

                    result.Data = customerDetails;

                    var invoiceList = DB.Invoices.Where(x => x.ContractorId == id).Select(x =>
                                new
                                {
                                    Invoice = new InvoiceListCustomerDTO
                                    {
                                        PaymentDate = x.PaymentDate,
                                        Date = x.IssueDate
                                    },
                                    InvoicePositions = x.InvoicePositions.Where(y => y.InvoiceId == x.Id).Select(t => new { t.VatRateId, t.NetValue })
                                })
                        .AsEnumerable().Select(x =>
                        {
                            x.Invoice.PositionsNetValuesWithTax =
                                x.InvoicePositions.ToDictionary(z => z.VatRateId, z => z.NetValue);
                            return x.Invoice;
                        })
                        .ToList();

                    result.Data.Invoices = invoiceList;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }



        public ResultModel<bool> CreateNewCustomer(string userId, CreateCustomerDTO dto)
        {
            var result = new ResultModel<bool>
            {
                Data = false
            };
            try
            {
                using (_dbContextScopeFactory.Create())
                {
                    var existCustomer =
                        DB.Contractors.Any(x => x.IsActive && x.UserId.Equals(userId) && x.NIP.Equals(dto.NIP));

                    if (existCustomer)
                    {
                        result.AddError("Posiadasz już dodanego klienta o takim numerze NIP");
                        return result;
                    }

                    var newCustomer = new Contractor()
                    {
                        Address = dto.Address,
                        UserId = userId,
                        City = dto.City,
                        Email = dto.Email,
                        NIP = dto.NIP,
                        CompanyName = dto.CompanyName,
                        IsActive = true,
                        PhoneNumber = dto.PhoneNumber,
                        PostalCode = dto.PostalCode
                    };
                    DB.Contractors.Add(newCustomer);
                    DB.SaveChanges();
                    result.Data = true;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }

            return result;
        }

        public ResultModel<bool> DeleteCustomer(string userId, int id)
        {
            var result = new ResultModel<bool>
            {
                Data = false
            };
            try
            {
                using (_dbContextScopeFactory.Create())
                {
                    var customer = DB.Contractors.FirstOrDefault(x => x.Id == id && x.UserId.Equals(userId));

                    if (customer == null)
                    {
                        result.SetNotFoundError();
                        return result;
                    }

                    customer.IsActive = false;

                    DB.Contractors.AddOrUpdate(customer);
                    DB.SaveChanges();
                    result.Data = true;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }

            return result;
        }

        public ResultModel<bool> UpdateCustomer(string userId, UpdateCustomerDTO dto)
        {
            var result = new ResultModel<bool>
            {
                Data = false
            };
            try
            {
                using (_dbContextScopeFactory.Create())
                {
                    var customer = DB.Contractors.FirstOrDefault(x => x.Id == dto.Id);

                    if (customer == null)
                    {
                        result.SetNotFoundError();
                        return result;
                    }

                    var existCustomer =
                        DB.Contractors.Any(x => x.IsActive && x.UserId.Equals(userId) && x.NIP.Equals(dto.NIP) && x.Id != dto.Id);

                    if (existCustomer)
                    {
                        result.AddError("Posiadasz już dodanego klienta o takim numerze NIP");
                        return result;
                    }

                    customer.Address = dto.Address;
                    customer.City = dto.City;
                    customer.Email = dto.Email;
                    customer.NIP = dto.NIP;
                    customer.CompanyName = dto.CompanyName;
                    customer.PhoneNumber = dto.PhoneNumber;
                    customer.PostalCode = dto.PostalCode;

                    DB.Contractors.AddOrUpdate(customer);
                    DB.SaveChanges();
                    result.Data = true;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }

            return result;
        }
    }
}
