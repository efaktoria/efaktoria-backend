﻿using eFaktoria.BLL.DB.Entities;
using eFaktoria.Core.EntityFramework.Infrastructure;
using eFaktoria.Model;
using eFaktoria.Model.DTO.InvoicePosition;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;

namespace eFaktoria.BLL.Services.InvoicePosistion
{
    public interface IInvoicePositionDictionaryManagementService
    {
        ResultModel<bool> Create(CreateInvoicePositionDictionaryDTO model, string userId);
        ResultModel<bool> Update(UpdateInvoicePositionDictionaryDTO model, string userId);
        ResultModel<List<InvoicePositionDictionaryListItemDTO>> GetList(string userId);
        ResultModel<bool> Delete(int id, string userId);
    }
    public class InvoicePositionDictionaryManagementService : GenericService, IInvoicePositionDictionaryManagementService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public InvoicePositionDictionaryManagementService(IAmbientDbContextLocator ambientDbContextLocator, IDbContextScopeFactory dbContextScopeFactory) : base(ambientDbContextLocator)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public ResultModel<bool> Create(CreateInvoicePositionDictionaryDTO model, string userId)
        {
            var result = new ResultModel<bool>();
            result.Data = false;

            try
            {
                using (_dbContextScopeFactory.Create())
                {
                    var invoicePosition = new InvoicePositionDictionary
                    {
                        NetPrice = model.NetPrice,
                        Count = model.Count,
                        IsActive = true,
                        Name = model.Name,
                        NetValue = model.NetPrice * model.Count,
                        Unit = model.Unit,
                        VatRateId = model.VatRateId,
                        UserId = userId
                    };


                    DB.InvoicePositionDictionaries.Add(invoicePosition);
                    DB.SaveChanges();
                    result.Data = true;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        public ResultModel<bool> Update(UpdateInvoicePositionDictionaryDTO model, string userId)
        {
            var result = new ResultModel<bool>();
            result.Data = false;
            try
            {
                using (_dbContextScopeFactory.Create())
                {

                    var invoicePosition =
                        DB.InvoicePositionDictionaries.FirstOrDefault(
                            x => x.IsActive && x.UserId.Equals(userId) && x.Id == model.Id);

                    if (invoicePosition == null)
                    {
                        result.SetNotFoundError();
                        return result;
                    }

                    invoicePosition.NetPrice = model.NetPrice;
                    invoicePosition.Count = model.Count;
                    invoicePosition.Name = model.Name;
                    invoicePosition.NetValue = model.NetPrice * model.Count;
                    invoicePosition.Unit = model.Unit;
                    invoicePosition.VatRateId = model.VatRateId;
                    invoicePosition.UserId = userId;

                    DB.InvoicePositionDictionaries.AddOrUpdate(invoicePosition);
                    DB.SaveChanges();
                    result.Data = true;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;

        }

        public ResultModel<List<InvoicePositionDictionaryListItemDTO>> GetList(string userId)
        {
            var result = new ResultModel<List<InvoicePositionDictionaryListItemDTO>>();
            try
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var list = DB.InvoicePositionDictionaries.Where(x => x.UserId.Equals(userId) && x.IsActive).Select(
                        x => new InvoicePositionDictionaryListItemDTO
                        {
                            Id = x.Id,
                            VatRateId = x.VatRateId,
                            Name = x.Name,
                            NetPrice = x.NetPrice,
                            Unit = x.Unit,
                            Count = x.Count
                        }).ToList();
                    result.Data = list;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        public ResultModel<bool> Delete(int id, string userId)
        {
            var result = new ResultModel<bool>();
            result.Data = false;

            try
            {
                using (_dbContextScopeFactory.Create())
                {
                    var invoicePosition =
                        DB.InvoicePositionDictionaries.FirstOrDefault(x => x.Id == id && x.UserId.Equals(userId) && x.IsActive);

                    if (invoicePosition == null)
                    {
                        result.SetNotFoundError();
                        return result;
                    }

                    invoicePosition.IsActive = false;

                    DB.InvoicePositionDictionaries.AddOrUpdate(invoicePosition);
                    DB.SaveChanges();
                    result.Data = true;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }
    }
}
