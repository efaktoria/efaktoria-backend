﻿using eFaktoria.BLL.DB.Entities;
using eFaktoria.Core.EntityFramework.Infrastructure;
using eFaktoria.Model;
using eFaktoria.Model.DTO.Company;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Linq;

namespace eFaktoria.BLL.Services.Company
{
    public interface IBankAccountService
    {
        ResultModel<bool> CreateBankAccount(CreateBankAccountDTO model, string userId);
        ResultModel<bool> DeleteBankAccount(int id, string userId);
        ResultModel<List<BankAccountListItemDTO>> GetList(string userId);
        ResultModel<bool> SetToMain(int id, string userId);
    }
    public class BankAccountService : GenericService, IBankAccountService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public BankAccountService(
            IAmbientDbContextLocator ambientDbContextLocator,
            IDbContextScopeFactory dbContextScopeFactory
        ) : base(ambientDbContextLocator)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
        }


        public ResultModel<bool> CreateBankAccount(CreateBankAccountDTO model, string userId)
        {
            var result = new ResultModel<bool>();

            try
            {
                using (var transaction = _dbContextScopeFactory.CreateWithTransaction(
                    IsolationLevel.Serializable))
                {
                    DB.CheckTransaction(result, transaction, () =>
                    {
                        var bankAccount = new AccountBank
                        {
                            IsMain = model.IsMain,
                            BankId = model.BankId,
                            IsActive = true,
                            Number = model.AccountNumber,
                            UserId = userId
                        };
                        ChangeMainAccount(model, userId);
                        DB.AccountBank.Add(bankAccount);
                        result.Data = true;
                    });
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        private void ChangeMainAccount(CreateBankAccountDTO model, string userId)
        {
            if (model.IsMain)
            {
                var bankAccount = DB.AccountBank.FirstOrDefault(x => x.UserId == userId && x.IsMain && x.IsActive);

                if (bankAccount != null)
                {
                    bankAccount.IsMain = false;
                    DB.AccountBank.AddOrUpdate(bankAccount);
                }
            }
        }

        public ResultModel<bool> DeleteBankAccount(int id, string userId)
        {
            var result = new ResultModel<bool>();
            try
            {
                result.Data = false;
                using (var transaction = _dbContextScopeFactory.CreateWithTransaction(
                    IsolationLevel.Serializable))
                {
                    DB.CheckTransaction(result, transaction, () =>
                    {
                        CheckAccess(result, userId, id);

                        var bankAccount = DB.AccountBank.Find(id);

                        if (bankAccount == null)
                        {
                            result.SetNotFoundError();
                            return;
                        }

                        SetMainAccount(bankAccount);

                        bankAccount.IsActive = false;
                        bankAccount.IsMain = false;
                        DB.AccountBank.AddOrUpdate(bankAccount);

                        result.Data = true;
                    });
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        private void SetMainAccount(AccountBank bankAccount)
        {
            if (bankAccount.IsMain)
            {
                var allAcounts = DB.AccountBank.Where(x => x.UserId == bankAccount.UserId && x.IsActive);
                if (allAcounts.Count() == 2)
                {
                    var newMainAccount = allAcounts.FirstOrDefault(x => !x.IsMain);

                    if (newMainAccount == null)
                    {
                        throw new ArgumentException(nameof(newMainAccount));
                    }

                    newMainAccount.IsMain = true;
                    DB.AccountBank.AddOrUpdate(newMainAccount);

                }

            }
        }

        public ResultModel<List<BankAccountListItemDTO>> GetList(string userId)
        {
            var result = new ResultModel<List<BankAccountListItemDTO>>();
            try
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var list = DB.AccountBank.Where(x => x.UserId.Equals(userId) && x.IsActive).Select(x =>
                        new BankAccountListItemDTO
                        {
                            AccountNumber = x.Number,
                            BankName = x.Bank.Name,
                            Id = x.Id,
                            IsMain = x.IsMain
                        }
                    ).ToList();

                    result.Data = list;
                }

            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        public ResultModel<bool> SetToMain(int id, string userId)
        {
            var result = new ResultModel<bool> { Data = false };
            try
            {
                using (var transaction = _dbContextScopeFactory.CreateWithTransaction(
                    IsolationLevel.Serializable))
                {
                    DB.CheckTransaction(result, transaction, () =>
                    {
                        CheckAccess(result, userId, id);
                        var oldMainAccount =
                            DB.AccountBank.FirstOrDefault(x => x.IsActive && x.UserId == userId && x.IsMain);
                        if (oldMainAccount != null)
                        {
                            oldMainAccount.IsMain = false;
                            DB.AccountBank.AddOrUpdate(oldMainAccount);
                        }

                        var newMainAccount = DB.AccountBank.Find(id);
                        if (newMainAccount == null)
                        {
                            result.SetNotFoundError();
                            return;
                        }
                        newMainAccount.IsMain = true;
                        DB.AccountBank.AddOrUpdate(newMainAccount);
                    });
                    result.Data = true;
                }
            }
            catch (Exception)
            {

                result.SetCriticalError();
            }
            return result;
        }

        private void CheckAccess<T>(ResultModel<T> result, string userId, int bankAccountId)
        {
            var bankAccount = DB.AccountBank.Find(bankAccountId);

            if (bankAccount == null)
            {
                return;
            }

            if (bankAccount.UserId != userId)
            {
                result.SetAccessError();
            }
        }
    }
}

