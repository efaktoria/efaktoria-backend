﻿using eFaktoria.BLL.Helpers;
using eFaktoria.Core.EntityFramework.Infrastructure;
using eFaktoria.Model;
using eFaktoria.Model.DTO.Company;
using System;
using System.Data.Entity.Migrations;
using System.Linq;

namespace eFaktoria.BLL.Services.Company
{
    public interface ICompanyProfileService
    {
        ResultModel<CompanyDataDTO> GetCompanyData(string userId);
        ResultModel<bool> UpdateCompanyData(string userId, CompanyUpdateDTO dto);
    }

    public class CompanyProfileService : GenericService, ICompanyProfileService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public CompanyProfileService(
            IAmbientDbContextLocator ambientDbContextLocator,
            IDbContextScopeFactory dbContextScopeFactory
        ) : base(ambientDbContextLocator)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public ResultModel<CompanyDataDTO> GetCompanyData(string userId)
        {
            var result = new ResultModel<CompanyDataDTO>();
            try
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var data = DB.Users.Where(x => x.Id == userId).Select(x => new CompanyDataDTO
                    {
                        Address = x.Address,
                        City = x.City,
                        CompanyName = x.CompanyName,
                        NIP = x.NIP,
                        PostalCode = x.PostalCode
                    }).FirstOrDefault();

                    result.Data = data;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        public ResultModel<bool> UpdateCompanyData(string userId, CompanyUpdateDTO dto)
        {
            var result = new ResultModel<bool>()
            {
                Data = false
            };
            try
            {
                using (_dbContextScopeFactory.Create())
                {
                    var company = DB.Users.FirstOrDefault(x => x.Id == userId);

                    if (company == null)
                    {
                        result.SetNotFoundError();
                        return result;
                    }

                    ValidateNIP(dto, company, result);

                    if (result.HasError)
                    {
                        return result;
                    }

                    company.Address = dto.Address;
                    company.City = dto.City;
                    company.CompanyName = dto.CompanyName;
                    company.PostalCode = dto.PostalCode;

                    DB.Users.AddOrUpdate(company);
                    DB.SaveChanges();
                    result.Data = true;
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        private static void ValidateNIP(CompanyUpdateDTO dto, ApplicationUser company, ResultModel<bool> result)
        {
            if (string.IsNullOrWhiteSpace(company.NIP))
            {
                if (string.IsNullOrEmpty(dto.NIP))
                {
                    result.AddError("Wymagane jest podanie numeru NIP");
                    return;
                }

                if (!CheckNIPHelper.IsValid(dto.NIP))
                {
                    result.AddError("Podany numer NIP nie jest prawidłowy");
                    return;
                }
                company.NIP = dto.NIP;
            }
        }
    }
}
