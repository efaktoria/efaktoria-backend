﻿using eFaktoria.Core.EntityFramework.Infrastructure;
using eFaktoria.Model;
using System;
using System.Linq;

namespace eFaktoria.BLL.Services
{
    public interface ITestService
    {
        ResultModel<string> GetSomethingFromDb();
    }

    public class TestService : GenericService, ITestService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public TestService(
            IAmbientDbContextLocator ambientDbContextLocator,
            IDbContextScopeFactory dbContextScopeFactory
        ) : base(ambientDbContextLocator)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public ResultModel<string> GetSomethingFromDb()
        {
            var result = new ResultModel<string>();
            try
            {
                using (_dbContextScopeFactory.CreateReadOnly())
                {
                    var a =
                        DB.Users.Select(x => x.Email).FirstOrDefault();
                    result.Data = a;
                }

            }
            catch (Exception)
            {
                result.SetCriticalError();
                result.AddError("błąd");
            }
            return result;
        }
    }
}
