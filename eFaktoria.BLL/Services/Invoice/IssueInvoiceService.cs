﻿using eFaktoria.BLL.DB.Entities;
using eFaktoria.Core.EntityFramework.Infrastructure;
using eFaktoria.Model;
using eFaktoria.Model.DTO.Invoices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace eFaktoria.BLL.Services.Invoice
{
    public interface IIssueInvoiceService
    {
        ResultModel<bool> CreateInvoice(CreateInvoiceDTO model, string userId);
    }

    public class IssueInvoiceService : GenericService, IIssueInvoiceService
    {
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public IssueInvoiceService(IAmbientDbContextLocator ambientDbContextLocator, IDbContextScopeFactory dbContextScopeFactory) : base(ambientDbContextLocator)
        {
            _dbContextScopeFactory = dbContextScopeFactory;
        }

        public ResultModel<bool> CreateInvoice(CreateInvoiceDTO model, string userId)
        {
            var result = new ResultModel<bool>();
            result.Data = false;
            try
            {
                using (var transaction = _dbContextScopeFactory.CreateWithTransaction(IsolationLevel.Serializable))
                {
                    DB.CheckTransaction(result, transaction, () =>
                    {
                        var contractorData = GetContractorData(model, result);

                        if (!result.HasError)
                        {
                            return;
                        }

                        var newInvoice = DTOToInvoice(model, userId, contractorData);

                        DB.Invoices.Add(newInvoice);

                        CreateInvoicePositions(model.Positions, newInvoice);
                    });
                    if (!result.HasError)
                    {
                        result.Data = true;
                    }
                }
            }
            catch (Exception)
            {
                result.SetCriticalError();
            }
            return result;
        }

        private static DB.Entities.Invoice DTOToInvoice(CreateInvoiceDTO model, string userId, InvoiceContractorDataDTO contractorData)
        {
            var newInvoice = new DB.Entities.Invoice
            {
                UserId = userId,
                AccountBankId = model.AccountBankId,
                AuthorizedPerson = model.AuthorizedPerson,
                City = model.City,
                ContractorAddress = contractorData.Address,
                ContractorId = model.ContractorId,
                ContractorName = contractorData.CompanyName,
                ContractorNIP = contractorData.NIP,
                ContractorPostalCode = contractorData.PostalCode,
                CurrencyId = model.CurrencyId,
                IsActive = true,
                IsPaid = model.IsPaid,
                PaymentDate = model.PaymentDate,
                PaymentMethodId = model.PaymentMethodId,
                SellDate = model.SellDate,
                InvoiceTypeId = model.InvoiceTypeId,
                Remarks = model.Remarks,
                Title = model.Title,
                IssueDate = model.IssueDate
            };
            return newInvoice;
        }

        private InvoiceContractorDataDTO GetContractorData(CreateInvoiceDTO model, ResultModel<bool> result)
        {
            var contractorData = DB.Contractors.Where(x => x.Id == model.ContractorId).Select(x => new InvoiceContractorDataDTO
            {
                Address = x.Address,
                CompanyName = x.CompanyName,
                NIP = x.NIP,
                City = x.City,
                PostalCode = x.PostalCode
            }).FirstOrDefault();

            if (contractorData == null)
            {
                result.SetNotFoundError();
            }
            return contractorData;
        }

        private void CreateInvoicePositions(List<InvoicePositionDTO> invoicePositions, DB.Entities.Invoice newInvoice)
        {
            var newPositions = new List<InvoicePosition>();
            foreach (var invoicePosition in invoicePositions)
            {
                var newInvoicePosition = new InvoicePosition
                {
                    Invoice = newInvoice,
                    NetPrice = invoicePosition.NetPrice,
                    Count = invoicePosition.Count,
                    Name = invoicePosition.Name,
                    NetValue = invoicePosition.NetValue,
                    Unit = invoicePosition.Unit,
                    VatRateId = invoicePosition.VatRateId
                };
                newPositions.Add(newInvoicePosition);
            }
            DB.InvoicePositions.AddRange(newPositions);
        }
    }
}
