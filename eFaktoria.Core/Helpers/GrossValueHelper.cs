﻿using System.Collections.Generic;
using System.Linq;

namespace eFaktoria.Core.Helpers
{
    public static class GrossValueHelper
    {
        /// <summary>
        /// Słownik z kwotami poszczególnych pozycji faktury z id stawki, której dotyczy
        /// </summary>
        public static decimal GetValue(Dictionary<int, decimal> taxes)
        {
            var vatRates = VATRateHelper.GetVATRates();

            decimal grossValue = 0;

            foreach (var item in taxes)
            {
                var currentRate = vatRates.FirstOrDefault(x => x.Key == item.Key);

                var grossValueItem = currentRate.Value * item.Value;

                grossValue = item.Value + grossValueItem + grossValue;
            }

            return grossValue;
        }
    }
}
