﻿using eFaktoria.Core.Enums;
using System.Collections.Generic;

namespace eFaktoria.Core.Helpers
{
    public static class VATRateHelper
    {
        /// <summary>
        /// Id stawki podatkowej, stawka procentowa
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, decimal> GetVATRates()
        {
            Dictionary<int, decimal> values = new Dictionary<int, decimal>
            {
                { (int)enuVATRate.Percent0, 0 },
                { (int)enuVATRate.Percent23, 0.23M },
                { (int)enuVATRate.Percent5, 0.05M },
                { (int)enuVATRate.Percent8, 0.08M },
                { (int)enuVATRate.TaxExempt, 0 },
                { (int)enuVATRate.oo, 0 },
            };

            return values;
        }

    }
}
