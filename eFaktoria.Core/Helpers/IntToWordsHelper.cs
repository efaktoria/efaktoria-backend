﻿using Humanizer;

namespace eFaktoria.Core.Helpers
{
    public static class IntToWordsHelper
    {
        public static string IntegerToWritten(int n)
        {
            var culture = System.Globalization.CultureInfo.GetCultureInfo("pl-PL");
            var result = n.ToWords(culture);
            return result;
        }
    }
}
