﻿namespace eFaktoria.Core.Enums
{
    public enum enuMaskSymbol
    {
        NumberWithZero = 500,
        NumberWithoutZero = 501,
        MonthWithZero = 502,
        MonthWithoutZero = 503,
        FullYear = 504,
        LastTwoCharsYear = 505
    }
}
