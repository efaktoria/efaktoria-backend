﻿namespace eFaktoria.Core.Enums
{
    public enum enuInvoiceTypes
    {
        /// <summary>
        /// Faktura VAT
        /// </summary>
        VAT = 300,
        /// <summary>
        /// Faktura proforma
        /// </summary>
        Pro = 301,
        /// <summary>
        /// Faktura marża
        /// </summary>
        MarkUp = 302
    }
}
